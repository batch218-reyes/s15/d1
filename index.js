console.log	 ("Hello, world!");
// enclosed with quotation mar strings data ("")

console. log ("Hello, world!");
// case sensitive 

console.
log
(
	"Hello, Everyone!"
)

// ; delimeter
// we use delimeter to end our code 

// [COMMENTS]
// -single line comments
// shortcut: ctrol+/

// I am a single line comment 

/* Multi-line comment */
/* 
	I am
	a
	multi line
	comment
*/
/* shortuct: ctrl+shift +/ */


// Syntax and statement


// statement in programming are instructions that we tell to computer to perform
// syntax in programming, it is the set of ruse that describe how statment must be considered

// variables
/* 
it is used to contain date

 	-Syntax in declaring variables
 	-let/const varialeName
*/

let myVariable = "hello" ;
		// assigmnet operatio (=)
console.log(myVariable);


// console.log	(hello); will result to not defined error

/*
	Guides in writing variables:
		1. Use the 'let' keyword followed by the variable name of you choosing and use the assisngment operator (=) to assign a value.
		2. Variable name shoud start with a lowercase character, use camelCase for multiple words.
		3. for constant variables, use the 'const' keyword
		4. Variable names should be indicative or descirptive of the value being stored/
		5. Never name a variable starting with numbers
		6. Refrain from using space in declaring a variable

*/

let	productName = 'desktop computer';
console.log	(productName);

let product = "Alvin's Computer"
console.log	(product);

// nunber
let	productPrice = 18999;
console.log(productPrice)

const interest = 3.539;
console.log(interest)

// Reassigning variables value
// syntax
	// variableName = newValue;

productName = "laptop";
console.log (productName);


let friend = "Kate";
friend = "Jane";
console.log(friend);

/* interest will not change because it's const

interest = 4.89;

*/

const pi = 3.14;

// Reassigning - a variable already have a value and we reassigning a new on
// Initialize - it is our first value

let supplier; // declaration
supplier ="John Smith Tradings"; //initializing
console.log(supplier);

supplier = "Zuitt Store";
console.log(supplier);

// Multiple variable declaration
let productCode = "DC017";
const productBrand = "Dell";
console.log(productCode, productBrand);

// using a varaiable with a reserve keyword
// const let = "hello";
// console.log(let);

// [SECTION] Data Types

// Strings
// strings are series of Characters taht create a word. Phrase, sentense, or anything realted to creeate text.
// strings in javascript is enclose with single ('') or double ("") quote
let	country = 'philippines';
let	province = "metro Manila";

console.log  (province + ',' + country);

let fullAddress = province + ',' + country;
console.log(fullAddress);


console.log("Philippines" + ',' + "Metro Manila");

// escape Characters (\)
// "\n" refers to creating a new line or set the text to next line;

console.log("line\nline2");


let	mailAddress = "Metro Manila\nPhilippines";
console.log(mailAddress);

let	message = "John's employee went home early"
console.log(message);

message = "John\'s employee went home early.";
console.log(message);

// numbers

// integers / whole Number
let	headcount =26;
console.log(headcount);

// Decimal numbers/float/ fraction
let	grade = 98.7;
console.log(grade);

// exponential Notation
let	planetDistance = 2e10
console.log(planetDistance);


// console.log ("Jhon's grade last quarter is "+ grade);
console.log ("Jhon's grade last quarter is "+ grade);


// Arrays
// it store multiple values with similar data type
let grades = [98.7, 95.4, 90.2, 94.6];
	// arrayName //elements
console.log(grades);

// diferent data type 
// strong different data types inse an array is nmot recomended because iw will not make sense in the content of programming.

let	details = ["John" , "Smith", 32, true];
console.log(details);

// objects 
// Objects are another special  kind of data type that is used to mimic real worl objects items.


// Syntax: 

/*
	let/const objectName ={
		propertyA: value.
		propertyB: value
	}
*/

let person = {
	fullname: "Juan Dela Cruz",
	// key / Propert
	age: 35,
	isMarried: false,
	contact: ["0912 345 678900" , "8123 7444"]
	// address: 
	// 	houseNumber: "3450"\
	// 	city: "Manila"
	};

console.log(person);


const myGrades = {
	firstGrading: 98.7,
	secondGrading: 95.4,
	thirdGrading: 90.2,
	fourthGrading: 94.6
};

console.log(myGrades);

let stringValue ="abcd";
let	numberValue =10;
let booleanValue = true;
let waterBills = [1000, 640, 700];
// myGrades as object

// 'type of' -
// we use type of oparator to retrive/ know the data type

console.log(typeof stringValue);  	
// output: sting
console.log(typeof numberValue); 
// output: number
console.log(typeof booleanValue); 
// output: boolean
console.log(typeof waterBills); 
// output: array (object)
console.log(typeof myGrades);
 // output: object 

// Constant Objects and Arrays
// we cannot reassign the value of the variable
// we can cha
const anime = ["Boruto", "One Piece", "Code Geas", "Monster", "Dan Machi", "Demon Slayer", "AOT", "Fairy Tale"];

// index - is the position ofthe element starting zero 

anime[0] ="Naruto";
console.log(anime);

// Null
// it is used to intentionally express the absense of a value 
let	spouse = null;
console.log(spouse);


// Undefined
// Represent the state of a variable that 		
let fullName;
console.log(fullName);